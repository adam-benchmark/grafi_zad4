<?php

namespace App\Form;

use App\Entity\Dzial;
use App\Entity\Pracownik;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EmployeeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('imie', TextType::class, [
                'label' => 'First Name',
                'required' => true,
            ])
            ->add('nazwisko', TextType::class, [
                'label' => 'Surename',
                'required' => true,
            ])
            ->add('pensja', IntegerType::class, [
                'label' => 'amount',
                'required' => true,
            ])
            ->add('dzial', EntityType::class, [
                'label' => 'dział',
                'class' => Dzial::class,
                'choice_label' => 'nazwa',
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Save it Now!',
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Pracownik::class,
        ));
    }
}