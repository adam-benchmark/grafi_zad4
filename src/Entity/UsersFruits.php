<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsersFruits
 *
 * @ORM\Table(name="users_fruits")
 * @ORM\Entity
 */
class UsersFruits
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="users_id", type="integer", nullable=false)
     */
    private $usersId;

    /**
     * @var int
     *
     * @ORM\Column(name="fruits_id", type="integer", nullable=false)
     */
    private $fruitsId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsersId(): ?int
    {
        return $this->usersId;
    }

    public function setUsersId(int $usersId): self
    {
        $this->usersId = $usersId;

        return $this;
    }

    public function getFruitsId(): ?int
    {
        return $this->fruitsId;
    }

    public function setFruitsId(int $fruitsId): self
    {
        $this->fruitsId = $fruitsId;

        return $this;
    }


}
