<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pracownik
 *
 * @ORM\Table(name="pracownik", indexes={@ORM\Index(name="pracownik_dzial_id_fk", columns={"dzial_id"})})
 * @ORM\Entity
 */
class Pracownik
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="imie", type="string", length=255, nullable=false)
     */
    private $imie;

    /**
     * @var string
     *
     * @ORM\Column(name="nazwisko", type="string", length=255, nullable=false)
     */
    private $nazwisko;

    /**
     * @var int
     *
     * @ORM\Column(name="pensja", type="integer", nullable=false)
     */
    private $pensja;

    /**
     * @var \Dzial
     *
     * @ORM\ManyToOne(targetEntity="Dzial")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dzial_id", referencedColumnName="id")
     * })
     */
    private $dzial;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImie(): ?string
    {
        return $this->imie;
    }

    public function setImie(string $imie): self
    {
        $this->imie = $imie;

        return $this;
    }

    public function getNazwisko(): ?string
    {
        return $this->nazwisko;
    }

    public function setNazwisko(string $nazwisko): self
    {
        $this->nazwisko = $nazwisko;

        return $this;
    }

    public function getPensja(): ?int
    {
        return $this->pensja;
    }

    public function setPensja(int $pensja): self
    {
        $this->pensja = $pensja;

        return $this;
    }

    public function getDzial(): ?Dzial
    {
        return $this->dzial;
    }

    public function setDzial(?Dzial $dzial): self
    {
        $this->dzial = $dzial;

        return $this;
    }


}
