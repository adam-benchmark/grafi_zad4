<?php

namespace App\Controller;

use App\Entity\Pracownik;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HRAddPersonController extends AbstractController
{
    const RANDOM_NAME = [
        0 => 'Adaśko',
        1 => 'Mariusz',
        2 => 'Dawid',
        3 => 'Patryk',
    ];

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $entityManager  = $this->getDoctrine()->getManager();
        /** @var Pracownik $pracownik */
        $pracownik = new Pracownik();
        $pracownik->setDzialId(rand(1,7));
        $pracownik->setImie(self::RANDOM_NAME[rand(0,3)]);
        $pracownik->setNazwisko('SureName_' . self::RANDOM_NAME[rand(0,3)]);
        $pracownik->setPensja(rand(2000,6000));
        $entityManager->persist($pracownik);
        $entityManager->flush();

        return $this->render('hr_add_person/index.html.twig', [
            'controller_name' => 'HRAddPersonController',
            'pracownik' => $pracownik,
        ]);
    }
}
