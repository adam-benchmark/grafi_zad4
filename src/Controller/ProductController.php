<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product")
     */
    public function index()
    {
        $entityManager  = $this->getDoctrine()->getManager();

        $product = new Product();
        $product->setName('Keyboard');
        $price = rand(1,10000);
        $product->setPrice($price);
        $product->setDescription('Ergonomic and stylish!');
        $entityManager->persist($product);
        $entityManager->flush();


        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'product_id' => $product->getId(),
        ]);
    }
}
