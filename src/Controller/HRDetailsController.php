<?php

namespace App\Controller;

use App\Entity\Pracownik;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HRDetailsController extends AbstractController
{
    const EMPLOYER_NOT_FOUND = 'Employer not found';

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index($id)
    {
        /** @var Pracownik $pracownik */
        $pracownik = $this->getDoctrine()
            ->getRepository(Pracownik::class)
            ->find($id);

        if (!$pracownik) {
            throw $this->createNotFoundException(
                self::EMPLOYER_NOT_FOUND . ' :'.$id
            );
        }

        return $this->render('hr_details/index.html.twig', [
            'data' => [
                'title' => 'HR | Details',
                'hr' => 'human-resources',
                'subcategory' => 'Details',
            ],
            'controller_name' => 'HRDetailsController',
            'pracownik' => $pracownik,
        ]);
    }
}
