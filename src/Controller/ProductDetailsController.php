<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProductDetailsController extends AbstractController
{
    /**
     * @Route("/product/{id}", name="product_details")
     */
    public function index($id)
    {
        /** @var Product $product */
        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->find($id);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        return $this->render('product_details/index.html.twig', [
            'title' => 'HR | Details',
            'controller_name' => 'ProductDetailsController',
            'product' => $product,
        ]);
    }
}
