<?php

namespace App\Controller;

use App\Entity\Pracownik;
use App\Form\EmployeeType;
use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class HRAddPersonViaFormController extends AbstractController
{
    /** @var bool */
    private $redirectToHRList;

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $form = $this->prepareForm($request);

        if ($this->redirectToHRList === true) {
            return $this->redirectToHRList();
        }

        return $this->renderCurrentView($form);
    }

    /**
     * @param $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function renderCurrentView($form)
    {
        return $this->render('hr_add_person_via_form/index.html.twig', [
            'data' => [
                'title' => 'HR | Add via Form',
                'hr' => 'human-resources',
                'subcategory' => 'Add New',
            ],
            'form' => $form->createView(),
            'controller_name' => 'HRAddPersonViaFormController',
        ]);
    }

    /**
     * @return \Symfony\Component\Form\FormInterface
     */
    private function prepareForm(Request $request)
    {
        $pracownik = new Pracownik();
        $form = $this->createForm(EmployeeType::class, $pracownik);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pracownik = $form->getData();
            $this->savePracownik($pracownik);
            $this->setFlagRedirectToHRList();
        }

        return $form;
    }

    /**
     * @param Pracownik $pracownik
     */
    private function savePracownik(Pracownik $pracownik)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($pracownik);
        $entityManager->flush();
    }

    private function setFlagRedirectToHRList()
    {
        $this->redirectToHRList = true;
    }

    private function redirectToHRList()
    {
        return $this->redirectToRoute('app_hr_list');
    }
}
