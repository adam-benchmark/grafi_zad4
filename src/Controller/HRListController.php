<?php

namespace App\Controller;

use App\Entity\Pracownik;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HRListController extends AbstractController
{
    const EMPLOYERS_NOT_FOUND = 'Employers not found';

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        /** @var Pracownik[] $pracownicy */
        $pracownicy = $this->getDoctrine()
            ->getRepository(Pracownik::class)
            ->findAll();

        if (!$pracownicy) {
            throw $this->createNotFoundException(
                self::EMPLOYERS_NOT_FOUND
            );
        }

        return $this->render('hr_list/index.html.twig', [
            'data' => [
                'title' => 'HR | List',
                'hr' => 'human-resources',
                'subcategory' => 'List',
            ],
            'controller_name' => 'HRListController',
            'pracownicy' => $pracownicy,
        ]);
    }
}
