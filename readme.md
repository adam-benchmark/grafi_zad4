##task4
Zaprojektuj niezbędne pliki z definicjami klas / funkcji / konfiguracje dla kontrolera działu
HR. Moduł aplikacji ma być dostępny w ścieżce /human-resources, wymagana
funkcjonalność to: wyświetlenie listy pracowników, podgląd danych pracownika, dodanie
nowego pracownika (jedynie nazwy metod wraz z argumentami i ewentualną dokumentacją,
celem zadania nie jest napisane kodu dla każdej funkcjonalności).
##Core
Symfony 4.1.6
##Dev info
###run server locally
```
php -S localhost:8000 -t public
```
###install doctrine in Symfony4
```
composer require symfony/orm-pack
composer require symfony/maker-bundle --dev
```
####configuration
```
# .env

# customize this line!
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name"

# to use sqlite:
# DATABASE_URL="sqlite:///%kernel.project_dir%/var/app.db"
```
In my case:
```
DATABASE_URL=mysql://root:root@localhost:3306/tc
```
####create database
```
 php bin/console doctrine:database:create
```
####chceck other options
```
php bin/console list doctrine
```
####building entity classes from an existing database
#####Building entity classes to annotation
```
php bin/console doctrine:mapping:import App\Entity annotation --path=src/Entity
```
#####Building entity classes to XML or YAML
```
php bin/console doctrine:mapping:import 'App\Entity' xml --path=config/doctrine
```
####Generating the Getters & Setters or PHP Classes
```
// generates getter/setter methods
 php bin/console make:entity --regenerate App
```
Notice:
If you want to have a OneToMany relationship, you will need to add it manually into the entity (e.g. add a comments property to BlogPost) or to the generated XML or YAML files. Add a section on the specific entities for one-to-many defining the inversedBy and the mappedBy pieces.

####create entity i.e. Product
```
php bin/console make:entity
```

###create new controller
```
php bin/console make:controller ProductController
```
###webpack for Symfony
```
1.
- composer require symfony/webpack-encore-pack
- yarn install
- nocite: you shold have yarn v1.12.1

2. npm install async
3. composer require encore

```
To build the assets, run:
```
 yarn encore dev
 yarn encore dev --watch
 yarn encore production
```


